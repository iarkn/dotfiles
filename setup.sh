#!/bin/sh -e

SCRIPT_DIR="$(cd $(dirname $0) && pwd)"
HOME_CFG="$HOME/.config"

# $1 - source
# $2 - destination path
symlink() {
	local source="${SCRIPT_DIR}/$1"
	local dest="${2:-$HOME/$1}"

	mkdir -p "$(dirname $dest)"

	if [ -L "$dest" ]; then
		echo "-- $dest: exists"
	elif [ -e "$dest" ]; then
		echo "-- $dest: exists but it is not a symlink"
	else
		ln -s "$source" "$dest"
		echo "-- linked '$1' -> '$dest'"
	fi
}

symlink .bashrc
symlink .tmux.conf
symlink .vimrc
symlink .vimrc "$HOME_CFG/nvim/init.vim"
symlink .vim/colors/iarkn.vim
symlink .vim/colors/iarkn.vim "$HOME_CFG/nvim/colors/iarkn.vim"
symlink .Xresources

symlink .config/emacs/init.el

symlink .config/foot
symlink .config/sway

symlink res/wallpaper.jpg "$HOME/Pictures/wallpaper.jpg"

case :$PATH:
	in *:$SCRIPT_DIR/bin:*) ;;
	*) echo "export PATH=\$PATH:$SCRIPT_DIR/bin" > "$HOME/.bashenv" ;;
esac
