(setq custom-file "~/.config/emacs/custom.el")

(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

;;; Package configuration

(use-package doom-themes
  :ensure t
  :config
  (setq doom-gruvbox-dark-variant "hard")
  (load-theme 'doom-gruvbox t))

(use-package evil
  :ensure t
  :config
  (evil-mode 1))

(use-package ido
  :config
  (ido-everywhere 1)
  (ido-mode 1))

(use-package paredit
  :ensure t
  :hook ((clojure-mode          . enable-paredit-mode)
         (emacs-lisp-mode       . enable-paredit-mode)
         (ielm-mode             . enable-paredit-mode)
         (lisp-mode             . enable-paredit-mode)
         (lisp-interaction-mode . enable-paredit-mode)
         (scheme-mode           . enable-paredit-mode))
  :config
  (show-paren-mode t))

(use-package ws-butler
  :ensure t
  :hook ((prog-mode . ws-butler-mode)))

;;; Editor configuration

(setq-default inhibit-startup-screen t
              initial-scratch-message nil)

(xterm-mouse-mode 1)
(menu-bar-mode -1)

(if (display-graphic-p)
  (progn
    (tool-bar-mode -1)
    (scroll-bar-mode -1)))

(set-frame-font "Iosevka Fixed 12" nil t)
(set-face-font 'fixed-pitch "Iosevka Fixed")
(set-face-font 'variable-pitch "Noto Sans")

(setq confirm-kill-emacs 'y-or-n-p)

(setq make-backup-files nil
      auto-save-default nil
      visual-bell nil)

(global-display-line-numbers-mode)

(setq display-line-numbers-type 'relative)
(setq column-number-mode 1)

(setq backward-delete-char-untabify-method 'hungry)

(setq-default tab-width 4
              indent-tabs-mode nil
              show-trailing-whitespace t)

(setq-default c-basic-offset 4
              c-default-style '((java-mode . "bsd")
                                (awk-mode . "awk")
                                (other . "bsd")))

(custom-set-faces
 `(font-lock-variable-name-face ((t (:foreground ,(doom-color 'fg)))))
 `(font-lock-preprocessor-face ((t (:foreground ,(doom-color 'cyan))))))
