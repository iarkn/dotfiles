case $- in
	*i*) ;;
	*) return ;;
esac

HISTCONTROL=ignoreboth
HISTSIZE=-1
HISTFILESIZE=-1

shopt -s histappend
shopt -s checkwinsize
shopt -s globstar
shopt -u progcomp

set_prompt() {
	PS1='\u@\h:\w$ '
	# Set title
	case "$TERM" in
		xterm*|rxvt*)
			PS1="\[\e]0;\w\a\]$PS1"
			;;
	esac
}

PROMPT_COMMAND=set_prompt

# Enable programmable completion features
if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

export EDITOR=vim
export GIT_EDITOR=$EDITOR
export GIT_PAGER=less
export LESS=-R
export GPG_TTY=$(tty)
export LC_ALL=C.UTF-8

export GOPROXY=direct
export GOSUMDB=off
export GOTELEMETRY=off

export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state
export XDG_CACHE_HOME=$HOME/.cache
export GTK_IM_MODULE=fcitx5
export QT_IM_MODULE=fcitx5
export XMODIFIERS=@im=fcitx5

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto --group-directories-first'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'

    alias diff='diff --color=auto'
fi

alias la='ls -lAh'
alias ..='cd ..'
alias -- -='cd -'

[ -s ~/.bashenv ]      && . ~/.bashenv
[ -s ~/.bash_aliases ] && . ~/.bash_aliases

if [ -d $XDG_CONFIG_HOME/nvm ]; then
    export NVM_DIR=$XDG_CONFIG_HOME/nvm
    [ -s $NVM_DIR/nvm.sh ] && . $NVM_DIR/nvm.sh
    [ -s $NVM_DIR/bash_completion ] && . $NVM_DIR/bash_completion
fi

[ -s $HOME/.cargo/env ] && . $HOME/.cargo/env

export PNPM_HOME="$XDG_DATA_HOME/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
