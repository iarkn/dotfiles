set nocompatible

" Plugins
" -------
call plug#begin()

Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'guns/vim-sexp'
Plug 'vim-autoformat/vim-autoformat'
Plug 'junegunn/vim-easy-align'
Plug 'lifepillar/vim-colortemplate'

Plug 'clojure-vim/clojure.vim'
Plug 'tikhomirov/vim-glsl'
Plug 'udalov/kotlin-vim'
Plug 'zah/nim.vim'
Plug 'iarkn/zig.vim'
Plug 'pangloss/vim-javascript'
Plug 'leafOfTree/vim-svelte-plugin'

let g:formatdef_tidy_user_xml =
	\ '"tidy -q -xml --force-output --tidy-mark no --vertical-space yes --indent auto --indent-spaces " . shiftwidth() . " --indent-attributes yes --wrap " . &tw'
let g:formatters_xml = ['tidy_user_xml']

call plug#end()

" General
" -------
set encoding=utf-8
set nobackup
set nowritebackup

set number
set relativenumber
set hidden

set laststatus=2
set cmdheight=1
set showcmd
set showmode

set cursorline
set cursorlineopt=number
set colorcolumn=81

set ttimeoutlen=50
set nofsync
set ttyfast

set list
set listchars=tab:>\ ,trail:-

" Status Line
" -----------
func! StatuslineGet(opt)
	let l:value = getbufvar(bufnr('%'), '&' . a:opt)
	return l:value ==# '' ? '' : '[' . l:value . ']'
endfunc

set statusline=
" file_path [file_type][file_encoding]
set statusline+=%f\ %{StatuslineGet('ft')}%{StatuslineGet('fenc')}
" [modified][read_only]
set statusline+=%m%r
" alignment separator
set statusline+=%=
" line:col-vcol line%
set statusline+=%-14.(%l:%c%V%)\ %P

" Navigation
" ----------
set mouse=a
set guicursor=

set nowrap
set whichwrap=<,>,[,]
set scrolloff=5
set sidescrolloff=8

set hlsearch
set incsearch
set ignorecase
set smartcase

set wildmenu

" Indentation
" -----------
set noexpandtab
set smarttab
set shiftwidth=8
set softtabstop=4
set tabstop=8

set autoindent
set smartindent

set cinoptions=g0,:0,t0,l1,N-s,E-s,m1,(s,k2s,w1,j1

let g:vim_indent_cont = &shiftwidth

" Mapping
" -------
let mapleader=','

nnoremap <leader>wh <C-w>h
nnoremap <leader>wj <C-w>j
nnoremap <leader>wk <C-w>k
nnoremap <leader>wl <C-w>l

nnoremap <silent> <leader>sT :-tabm<CR>
nnoremap <silent> <leader>st :+tabm<CR>
nnoremap <silent> <leader>n :noh<CR>

nnoremap <silent> <leader>p :set paste! paste?<CR>
vnoremap <silent> <leader>p :set paste! paste?<CR>

func! ToggleBg()
	if (&bg == "dark")
		set bg=light
	else
		set bg=dark
	endif
endfunc

nnoremap <silent> <leader>b :call ToggleBg()<CR>
vnoremap <silent> <leader>b :call ToggleBg()<CR>

nnoremap <silent> <leader>m :Vexplore %:h<CR>
vnoremap <silent> <leader>m :Vexplore %:h<CR>

nnoremap <leader>a <Plug>(EasyAlign)
vnoremap <leader>a <Plug>(EasyAlign)

nnoremap <silent> <leader>sf :Autoformat<CR>
nnoremap <silent> <leader>sl :AutoformatLine<CR>
vnoremap <silent> <leader>sf :Autoformat<CR>
vnoremap <silent> <leader>sl :AutoformatLine<CR>

" File Specifics
" --------------
let g:c_syntax_for_h = 1
let g:lisp_rainbow = 1
let g:clojure_align_subforms = 1

let g:netrw_banner = 0
let g:netrw_keepdir = 0
let g:netrw_winsize = 25

augroup userftdetect
	autocmd!
	autocmd BufRead,BufNewFile *.h setf c
	autocmd BufRead,BufNewFile *.vert,*.frag setf glsl

	autocmd FileType c,cpp,go setlocal noet sw=8 ts=8 fo=croq
	autocmd FileType c setlocal tw=80
	autocmd FileType cpp,zig setlocal cc=101 tw=100
	autocmd FileType meson,nim,haskell setlocal et sw=2 ts=2 sts=2
	autocmd FileType xml,html,css,svelte,javascript setlocal noet sw=2 ts=2 sts=2
	autocmd FileType kotlin,java setlocal et sw=4 ts=4 sts=4
	autocmd FileType javascript setlocal cino=
	autocmd FileType java,xml setlocal inde=
	autocmd FileType java
		\ setlocal cino=:0,t0,l1,m1,(0,ks,Ws,w1,j1
augroup end

" Colors
" ------
if (has('nvim') || &term =~ '256color') && has('termguicolors')
	set termguicolors
	if !has('nvim')
		let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
		let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
	endif
endif

set background=dark
colorscheme iarkn
